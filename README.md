# Scraping News

This is a Scrapy project to gather information from News sites. To run the codes, follow the same procedures of any Scrapy project. So, you need to install [Scrapy](https://scrapy.org/) and [Python](https://www.python.org/) for use.

### Examples
To list which spiders are available, type on root project folder:

```scrapy list```

After choose a spider, use the crawl command. For example, to run the spider called theguardian:

```scrapy crawl theguardian```

To save results in a JSON file and run the spider in a persistant mode (can pause and resume latter), use the parameters:

```scrapy crawl -s JOBDIR=crawls\theguardian -o output_json\theguardian.json theguardian```

Scrapy has a lot of parameters to customize the execution. Read the [Scrapy Documentation](https://doc.scrapy.org/en/latest/). It is easy to read and full of examples. 
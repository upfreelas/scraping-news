# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class EgyptianStreetsSpider(scrapy.Spider):
    name = 'egyptianstreets'
    allowed_domains = ['egyptianstreets.com']
    start_urls = ['https://egyptianstreets.com/category/news-politics-and-society/']

    def __init__(self, *args, **kwargs):
        self.PAGE = 1
        super(EgyptianStreetsSpider, self).__init__(*args, **kwargs)

    def parse(self, response):
        urls_sel = response.xpath('//*[@class="read-more-comment"]/a/@href')
        for url in urls_sel.extract():
            yield Request(response.urljoin(url),
                          callback=self.parse_news)
        self.PAGE += 1
        next_url = 'https://egyptianstreets.com/category/news-politics-and-society/page/%s' % self.PAGE
        yield Request(next_url)

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'Egyptian Streets')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('author', 'normalize-space(//*[@itemprop="author"])')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('body', '//*[@id="content-area"]/p/text()')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')        
        date_sel = response.xpath('//*[@class="post-date"]//*[@itemprop="datePublished"]/text()')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        try:
            date = datetime.strptime(date_str, '%B %d, %Y')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()
# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class TribuneSpider(CrawlSpider):
    name = 'tribune'
    allowed_domains = ['tribune.com.pk']
    start_urls = ['https://tribune.com.pk/latest-news/',
                  'http://www.tribune.com.pk/',
                  'https://tribune.com.pk/pakistan/',
                  'https://tribune.com.pk/business/',
                  'https://tribune.com.pk/technology/',
                  'https://tribune.com.pk/world/']
    deny = ['author/',
            'email/',
            'code-of-ethics/']
    rules = (
        Rule(LinkExtractor(allow=r'story\/\d+\/', deny=deny), callback='parse_news', follow=True),
    )

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'The Express Tribune')
        l.add_xpath('title', '//meta[@name="title"]/@content')
        l.add_xpath('title', '//meta[@name="twitter:title"]/@content')
        l.add_xpath('author', '//*[@class="author"]//a')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('body', '//*[contains(@class, "story-content")]/p[text()]')
        date_sel = response.xpath('//meta[@property="article:published_time"]/@content')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        date_str = date_str.split('+')[0]
        try:
            date = datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%S')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()

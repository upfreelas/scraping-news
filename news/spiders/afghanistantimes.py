# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class AfghanistanTimesSpider(scrapy.Spider):
    name = 'afghanistantimes'
    allowed_domains = ['www.afghanistantimes.af']
    start_urls = ['http://www.afghanistantimes.af/category/nation/',
                  'http://www.afghanistantimes.af/category/opinions/',
                  'http://www.afghanistantimes.af/category/world/',
                  'http://www.afghanistantimes.af/category/editorial/',
                  'http://www.afghanistantimes.af/blog/']

    def parse(self, response):
        urls_sel = response.xpath('//*[@class="post-box-title"]/a/@href')
        for url in urls_sel.extract():
            yield Request(response.urljoin(url),
                          callback=self.parse_news)
        next_sel = response.xpath('//*[@id="tie-next-page"]/a/@href')
        if next_sel:
            next_url = next_sel.extract_first()
            yield Request(response.urljoin(next_url))

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'Afghanistan Times')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('author', '//*[@class="post-meta-author"]/a')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('body', '//*[@class="entry"]/p')
        date_sel = response.xpath('//*[@class="post-meta"]/span[@class="tie-date"]/text()')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        date_str = date_str.split('+')[0]
        try:
            date = datetime.strptime(date_str, '%B %d, %Y')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        yield l.load_item()

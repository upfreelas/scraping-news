# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class TehranTimesSpider(scrapy.Spider):
    name = 'tehrantimes'
    allowed_domains = ['tehrantimes.com']
    start_urls = ['https://www.tehrantimes.com/page/archive.xhtml']

    def parse(self, response):
        urls_sel = response.xpath('//*[@class="third-news"]//li/a/@href')
        for url in urls_sel.extract():
            pass
            yield Request(response.urljoin(url),
                          callback=self.parse_news)
        next_sel = response.xpath('//*[contains(@class, "pagination")]//a[text()="Next"]/@href')
        if next_sel:
            next_url = next_sel.extract_first()
            print(response.urljoin(next_url))
            yield Request(response.urljoin(next_url))

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'Tehran Times')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('title', '//*[@id="item"]//*[@itemprop="headline"]/text()')
        l.add_xpath('author', '//*[@rel="author"]/@href')
        l.add_xpath('images', '//*[@class="item-body"]//figure/a/@href')
        l.add_xpath('body', '//*[@class="item-body"]//*[@class="item-text"]/p')
        l.add_xpath('description', '//meta[@name="description"]/@content')
        date_sel = response.xpath('//*[contains(@class, "item-date")]/text()')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        try:
            date = datetime.strptime(date_str, '%B %d, %Y')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()

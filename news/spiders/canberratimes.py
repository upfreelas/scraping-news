# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class CanberraTimesSpider(CrawlSpider):
    name = 'canberratimes'
    allowed_domains = ['canberratimes.com.au']
    start_urls = ['https://www.canberratimes.com.au/',
                  'https://www.canberratimes.com.au/canberra-news',
                  'https://www.canberratimes.com.au/politics',
                  'https://www.canberratimes.com.au/business',
                  'https://www.canberratimes.com.au/world',
                  'https://www.canberratimes.com.au/national',
                  'https://www.canberratimes.com.au/opinion',
                  'https://www.canberratimes.com.au/sport',
                  'https://www.canberratimes.com.au/entertainment',
                  'https://www.canberratimes.com.au/lifestyle',
                  'https://www.canberratimes.com.au/money',
                  'https://www.canberratimes.com.au/education',
                  'https://www.canberratimes.com.au/healthcare',
                  'https://www.canberratimes.com.au/environment',
                  'https://www.canberratimes.com.au/technology']
    deny = [r'books\/'] 
    rules = (
        Rule(LinkExtractor(allow=r'\d{8}-\S+.html$', deny=deny), callback='parse_news', follow=True),
    )

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'CanberraTimes')
        l.add_xpath('title', '//meta[@name="title"]/@content')
        l.add_xpath('title', '//*[@id="content"]//header//h1')
        l.add_xpath('author', '//meta[@name="author"]/@content')
        l.add_xpath('author', '//*[starts-with(@title, "Articles")]')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('body', '//*[@id="content"]//section//p[not(@*)]')
        l.add_xpath('description', '//meta[@name="description"]/@content')
        date_sel = response.xpath('//meta[@name="pubdate"]/@content')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        date_str = date_str.split('+')[0]
        try:
            date = datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%S')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()
# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class EkathimeriniSpider(scrapy.Spider):
    name = 'ekathimerini'
    allowed_domains = ['ekathimerini.com']
    start_urls = ['http://www.ekathimerini.com/search?q=&type=&edition=&author=&fromDate=&toDate=&t=0&s=p#searchFull']

    def parse(self, response):
        urls_sel = response.xpath('//*[@id="searchResults"]/li/a/@href')
        for url in urls_sel.extract():
            pass
            yield Request(response.urljoin(url),
                          callback=self.parse_news)
        next_sel = response.xpath('(//a[@title="Next"]/@href)[last()]')
        if next_sel:
            next_url = next_sel.extract_first()
            yield Request(response.urljoin(next_url))

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'Express')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('body', '//*[@class="freetext"]/p')
        l.add_xpath('title', '//h2[@class="item-title"]')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('author', '//article[@id="item-article"]//*[@class="item-author"]/a')
        l.add_xpath('author', '//meta[@name="author"]/@content')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')
        l.add_xpath('description', '//meta[@name="description"]/@content')
        date_sel = response.xpath('//header[@id="page-header"]//time/@datetime')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        try:
            date = datetime.strptime(date_str, '%Y-%m-%d')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()
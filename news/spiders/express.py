# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class ExpressSpider(CrawlSpider):
    name = 'express'
    allowed_domains = ['express.co.uk']
    start_urls = ['https://www.express.co.uk/']
    allow = [r'news\/',
             r'finance\/',
             r'sport\/',
             r'showbiz\/']
    rules = (
        Rule(LinkExtractor(allow=allow), callback='parse_news', follow=True),
    )

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'Express')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('body', '//*[@class="text-description"]/p')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('title', '//h1[@itemprop="headline"]')
        l.add_xpath('author', '//meta[@name="author"]/@content')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')
        l.add_xpath('description', '//meta[@name="description"]/@content')
        date_sel = response.xpath('//meta[@itemprop="datepublished"]/@content')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        try:
            date = datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%SZ')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))

        return l.load_item()

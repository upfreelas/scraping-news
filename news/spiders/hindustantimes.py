# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class HindustanTimesSpider(scrapy.Spider):
    name = 'hindustantimes'
    allowed_domains = ['hindustantimes.com']
    start_urls = ['https://www.hindustantimes.com/latest-news/']

    def parse(self, response):
        urls_sel = response.xpath('//*[@class="media-body"]//a/@href')
        for url in urls_sel.extract():
        	yield Request(response.urljoin(url),
        				  callback=self.parse_news)
        next_sel = response.xpath('//*[contains(@class, "pagination")]//*[text()="Next"]/parent::a/@href')
        if next_sel:
            next_url = next_sel.extract_first()
            yield Request(response.urljoin(next_url))

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('source', 'Hindustan Times')
        l.add_value('url', response.url)
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('title', '//*[@class="story-highlight"]/h1')
        l.add_xpath('author', '//*[@class="para-txt"]//span')
        l.add_xpath('description', '//meta[@name="Description"]/@content')
        l.add_xpath('body', '//*[@class="story-details"]/p[text()]')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        images_url = response.xpath('//*[@class="story-content"]//img/@src')
        if images_url:
            for img in images_url.extract():
                l.add_value('images', response.urljoin(img))
        date_sel = response.xpath('//meta[@property="article:published_time"]/@content')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        try:
            date = datetime.strptime(date_str, '%Y-%m-%dT%H:%MZ')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()

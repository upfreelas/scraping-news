# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class DawnSpider(CrawlSpider):
    name = 'dawn'
    allowed_domains = ['dawn.com']
    start_urls = ['http://www.dawn.com/',
                  'https://www.dawn.com/latest-news',
                  'https://www.dawn.com/business',
                  'https://www.dawn.com/sport',
                  'https://www.dawn.com/opinion',
                  'https://www.dawn.com/pakistan']
    deny=['authors/',
          'news/print/']
    deny_domains=['classifieds.dawn.com',
                  'educationexpo.dawn.com',
                  'obituary.dawn.com',
                  'herald.dawn.com',
                  'aurora.dawn.com',
                  'epaper.dawn.com',
                  'images.dawn.com']
    rules = (
        Rule(LinkExtractor(allow=r'news\/\d*\/', deny=deny, deny_domains=deny_domains), callback='parse_news', follow=True),
    )

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'DAWN')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('author', '//meta[@name="author"]/@content')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')
        images_url = response.xpath('//*[@class="story-content"]//img/@src')
        if images_url:
            for img in images_url.extract():
                l.add_value('images', response.urljoin(img))
        l.add_xpath('body', '//*[contains(@class, "story__content")]/p[text()]')
        date_sel = response.xpath('//meta[@property="article:published_time"]/@content')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        try:
            date = datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()

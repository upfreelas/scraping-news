# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class BusinessDailyAfricaSpider(CrawlSpider):
    name = 'businessdailyafrica'
    allowed_domains = ['www.businessdailyafrica.com']
    start_urls = ['https://www.businessdailyafrica.com/']
    deny = [r'author-profile\/']
    rules = (
        Rule(LinkExtractor(allow=r'view-asAuthor-\S{6,9}\/index\.html$'),
             callback='parse_viewasauthor',
             follow=True),
        Rule(LinkExtractor(allow=r'\/\S{6,9}-\S{6,9}-\S{6,9}\/index\.html$',
                           deny=deny),
             callback='parse_news',
             follow=True),
    )

    def parse_viewasauthor(self, response):
        urls_sel = response.xpath('//*[@class="article-list-featured-title"]/a/@href')
        for url in urls_sel.extract():
            yield Request(response.urljoin(url),
                          callback=self.parse_news)

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'Business Daily Africa')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('author', '//*[@class="article-meta-summary"]/strong')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')
        l.add_xpath('description', '//meta[@name="description"]/@content')
        l.add_xpath('images', '//*[@property="og:image"]/@content')
        l.add_xpath('body', '//*[@class="article-story page-box"]//div/p')
        date_sel = response.xpath('//meta[@itemprop="datePublished"]/@content')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        try:
            date = datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))

        return l.load_item()

# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class PragueMonitorSpider(scrapy.Spider):
    name = 'praguemonitor'
    allowed_domains = ['praguemonitor.com']
    start_urls = ['http://praguemonitor.com/news/']

    def parse(self, response):
        urls_sel = response.xpath('//*[@class="views-field-title"]//a[starts-with(@href, "/")]/@href')
        for url in urls_sel.extract():
            yield Request(response.urljoin(url),
                          callback=self.parse_news)
        next_sel = response.xpath('//*[@class="pager-next last"]/a/@href')
        if next_sel:
            next_url = next_sel.extract_first()
            yield Request(response.urljoin(next_url))

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('source', 'Prague Monitor')
        l.add_value('url', response.url)
        l.add_xpath('title', '//*[@id="content-header"]//*[@class="title"]')
        l.add_xpath('author', '//meta[@name="author"]/@content')
        date_sel = response.xpath('//*[@class="date-display-single"]/text()')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        try:
            date = datetime.strptime(date_str, '%d %B %Y')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        l.add_xpath('body', '//*[@id="content-area"]//p')
        return l.load_item()

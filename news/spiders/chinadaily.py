# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime
import json


class ChinaDailySpider(scrapy.Spider):
    name = 'chinadaily'
    allowed_domains = ['chinadaily.com.cn']
    start_urls = ['http://newssearch.chinadaily.com.cn/rest/en/search?sort=dp&page=0']

    def __init__(self, *args, **kwargs):
        self.PAGE = 0
        super(ChinaDailySpider, self).__init__(*args, **kwargs)

    def parse(self, response):
        json_news = json.loads(response.body_as_unicode())
        for news in json_news['content']:
            if not news['jsonUrl']:
                continue
            l = ItemLoader(item=NewsItem())
            l.add_value('source', 'China Daily')
            l.add_value('title', news['highlightTitle'])
            l.add_value('author', news['authors'])
            l.add_value('description', news['summary'])
            l.add_value('url', news['url'])
            if news['images']:
                images_url = [i['url'] for i in news['images']]
                l.add_value('images', images_url)
            l.add_value('body', news['plainText'])
            date_str = news['pubDateStr']
            if not date_str:
                # Will be dropped on the item pipeline
                return l.load_item()
            try:
                date = datetime.strptime(date_str, '%Y-%m-%d %H:%M')
            except ValueError:
                # Will be dropped on the item pipeline
                return l.load_item()
            l.add_value('date', date.strftime('%m/%d/%Y'))
            yield l.load_item()

        self.PAGE += 1
        next_url = 'http://newssearch.chinadaily.com.cn/rest/en/search?sort=dp&page=%s' % self.PAGE
        yield Request(next_url)
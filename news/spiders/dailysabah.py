# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class DailySabahSpider(CrawlSpider):
    name = 'dailysabah'
    allowed_domains = ['dailysabah.com']
    start_urls = ['https://www.dailysabah.com/']

    rules = (
        Rule(LinkExtractor(allow=r'\d{4}\/\d{2}\/\d{2}\/\S+$'), callback='parse_news', follow=True),
    )
    
    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'Daily Sabah')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('title', '//meta[@itemprop="name"]/@content')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')
        l.add_xpath('description', '//meta[@itemprop="Description"]/@content')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('author', '//*[@class="yazar"]/strong')
        l.add_xpath('body', '//*[@class="txtInWrapper"]/p')
        l.add_xpath('body', '//*[@class="txtInWrapper"]/div/p')
        date_sel = response.xpath('//meta[@itemprop="datePublished"]/@content')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        date_str = date_str.split('+')[0]
        try:
            date = datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%S')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()

# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class KyivPostSpider(scrapy.Spider):
    name = 'kyivpost'
    allowed_domains = ['kyivpost.com']
    start_urls = ['https://www.kyivpost.com/all-news/']

    def parse(self, response):
        urls_sel = response.xpath('//*[@class="post-excerpt post-excerpt-simple grid-3"]/a/@href')
        for url in urls_sel.extract():
            yield Request(response.urljoin(url),
                          callback=self.parse_news)
        next_sel = response.xpath('//*[@class="link-next "]/@href')
        if next_sel:
            next_url = next_sel.extract_first()
            yield Request(response.urljoin(next_url))

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('source', 'Kyiv Post')
        l.add_value('url', response.url)
        l.add_xpath('title', '//*[@class="post-title"]')
        l.add_xpath('author', '//*[@class="pm-item"]/a')
        l.add_xpath('images', '//*[@name="twitter:image"]/@content')
        l.add_xpath('body', '//*[contains(@class, "content-block")]/p')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')
        date_sel = response.xpath('normalize-space(//*[@class="post-meta"]//time/@datetime)')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        date_str = date_str.split('+')[0]
        try:
            date = datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%S')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()

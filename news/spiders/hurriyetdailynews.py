# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class HurriyetDailyNewsSpider(CrawlSpider):
    name = 'hurriyetdailynews'
    allowed_domains = ['hurriyetdailynews.com']
    start_urls = ['http://www.hurriyetdailynews.com/',
                  'http://www.hurriyetdailynews.com/turkey/',
                  'http://www.hurriyetdailynews.com/economy/',
                  'http://www.hurriyetdailynews.com/opinion/',
                  'http://www.hurriyetdailynews.com/world/',
                  'http://www.hurriyetdailynews.com/artsandlife/',
                  'http://www.hurriyetdailynews.com/sports/',
                  'http://www.hurriyetdailynews.com/search/turkey']

    rules = (
        Rule(LinkExtractor(allow=r'-\d+$'), callback='parse_news', follow=True),
    )

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'Hurriyet Daily News')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('body', '//*[@class="content"]/p[not(@*)]')
        l.add_xpath('title', '//*[@class="content"]/h1')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('author', '//meta[@name="author"]/@content')
        l.add_xpath('description', '//meta[@name="description"]/@content')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')
        date_sel = response.xpath('//*[@class="date"]/text()')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        try:
            date = datetime.strptime(date_str, '%B %d %Y %H:%M:%S')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        yield l.load_item()
        
# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class TelegraphindiaSpider(CrawlSpider):
    name = 'telegraphindia'
    allowed_domains = ['telegraphindia.com']
    start_urls = ['https://www.telegraphindia.com/',
                  'https://www.telegraphindia.com/search/india']

    rules = (
        Rule(LinkExtractor(allow=r'cid\/\d+$'), callback='parse_news', follow=True),
    )

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'The Telegraph - India')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('title', '//*[@class="mainStory-normal"]/h1')
        l.add_xpath('author', '//*[@class="author-name"]/a')
        l.add_xpath('author', '//*[@class="author-name"]/text()[2]')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('body', '//*[@class="padiingDetails"]/*[self::p or self::ul]')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')
        date_sel = response.xpath('//*[@class="date"]/following::text()[1]')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        try:
            date = datetime.strptime(date_str, '%d.%m.%y, %H:%M %p')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()

# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class TheNationSpider(scrapy.Spider):
    name = 'thenation'
    allowed_domains = ['thenationonlineng.net']
    start_urls = ['http://thenationonlineng.net/category/news/',
                  'http://thenationonlineng.net/category/politics/',
                  'http://thenationonlineng.net/category/columnists/',
                  'http://thenationonlineng.net/category/business/',
                  'http://thenationonlineng.net/category/sports2/sports-news/',
                  'http://thenationonlineng.net/category/health/',
                  'http://thenationonlineng.net/category/education-news-nigeria/']

    def parse(self, response):
        urls_sel = response.xpath('//*[@class="jeg_thumb"]/a/@href')
        for url in urls_sel.extract():
            yield Request(response.urljoin(url),
                         callback=self.parse_news)
        next_sel = response.xpath('//*[@class="page_nav next"]/@href')
        if next_sel:
            next_url = next_sel.extract_first()
            yield Request(response.urljoin(next_url))

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'The Nation')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('author', '//*[@class="jeg_meta_author"]//a')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('body', '//*[@class="content-inner "]/p[text()]')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')
        images_url = response.xpath('//*[contains(@class, "story-content")]//img/@src')
        if images_url:
            for img in images_url.extract():
                l.add_value('images', response.urljoin(img))
        date_sel = response.xpath('//meta[@property="article:published_time"]/@content')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        date_str = date_str.split('+')[0]
        try:
            date = datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%S')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()
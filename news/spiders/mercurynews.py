# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class MercuryNewsSpider(scrapy.Spider):
    name = 'mercurynews'
    allowed_domains = ['mercurynews.com']
    start_urls = ['https://www.mercurynews.com/latest-headlines/']

    def parse(self, response):
        urls_sel = response.xpath('//a[@class="article-title"]/@href')
        for url in urls_sel.extract():
            yield Request(response.urljoin(url),
                          callback=self.parse_news)
        next_sel = response.xpath('//*[@class="load-more"]/@href')
        if next_sel:
            next_url = next_sel.extract_first()
            yield Request(response.urljoin(next_url))

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('source', 'Mercury News')
        l.add_value('url', response.url)
        l.add_xpath('title', '//*[@property="og:title"]/@content')
        l.add_xpath('author', '//*[@class=" author-name"]')
        date_sel = response.xpath('//*[@class="article-content"]//time/@datetime')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        try:
            date = datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('body', '//*[@class="body-copy"]/p')
        l.add_xpath('description', '//header//*[@class="subheadline"]')
        return l.load_item()

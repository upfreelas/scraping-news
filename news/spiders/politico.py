# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class PoliticoSpider(CrawlSpider):
    name = 'politico'
    allowed_domains = ['politico.eu']
    start_urls = ['http://www.politico.eu/']

    rules = (
        Rule(LinkExtractor(allow=r'article\/'), callback='parse_news', follow=True),
    )

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'Politico EU')
        l.add_xpath('images', '//*[@property="og:image"]/@content')
        l.add_xpath('images', '//*[contains(@class, "story-text")]//img[starts-with(@class, "wp-image")]/@src')
        l.add_xpath('body', '//*[starts-with(@class, "story-text")]/p')
        l.add_xpath('title', '//*[@property="og:title"]/@content')
        l.add_xpath('title', '//h1[@class="ev-magazine-layout-title"]')
        l.add_xpath('author', '//*[@rel="author"]/text()')
        l.add_xpath('description', '//*[@property="og:description"]/@content')
        l.add_xpath('description', '//header/*[@class="subhead"]')
        date_sel = response.xpath('//*[@property="article:published_time"]/@content')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        date_str = date_str.split('+')[0]
        try:
            date = datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%S')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))

        return l.load_item()

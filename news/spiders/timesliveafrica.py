# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class TimesLiveAfricaSpider(scrapy.Spider):
    name = 'timesliveafrica'
    allowed_domains = ['timeslive.co.za']
    start_urls = ['https://www.timeslive.co.za/news/africa/']

    def __init__(self, *args, **kwargs):
        self.page = 1
        super(TimesLiveAfricaSpider, self).__init__(*args, **kwargs)


    def parse(self, response):
        urls_sel = response.xpath('//*[@id="section"]//a/@href')
        for url in urls_sel.extract():
            yield Request(response.urljoin(url),
                          callback=self.parse_news)
        self.page += 1
        next_url = 'https://www.timeslive.co.za/news/africa/?page=%s&limit=10' % self.page
        yield Request(response.urljoin(next_url))
        
    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'Times Live - Africa')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('title', '//meta[@class="article-title article-title-primary"]')
        l.add_xpath('author', '//*[@id="authors"]/@data-author')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        images_url = response.xpath('//div[@rel="image"]//img/@src')
        if images_url:
            for img in images_url.extract():
                l.add_value('images', response.urljoin(img))
        l.add_xpath('body', '//div[@rel="text"]//div[@class="text"]/p')
        date_sel = response.xpath('normalize-space(//*[@class="article-pub-date "]/text())')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        try:
            date = datetime.strptime(date_str, '%d %B %Y - %H:%M')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()

# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class TheCityPaperBogotaSpider(CrawlSpider):
    name = 'thecitypaperbogota'
    allowed_domains = ['thecitypaperbogota.com']
    start_urls = ['https://thecitypaperbogota.com/']
    allow_news = [r'bogota\/',
                  r'business\/',
                  r'culture\/',
                  r'dining\/',
                  r'living\/',
                  r'news\/',
                  r'opinion\/',
                  r'travel\/']
    rules = (
        Rule(LinkExtractor(allow=allow_news), callback='parse_news', follow=True),
    )

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'The City Paper - Colombia')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('body', '//*[@class="td-post-content"]/p')
        l.add_xpath('title', '//*[@class="entry-title"]')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('author', '//meta[@name="author"]/@content')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')
        l.add_xpath('description', '//meta[@name="twitter:description"]/@content')
        date_sel = response.xpath('//meta[@property="article:published_time"]/@content')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        date_str = date_str.rsplit('-', 1)[0]
        try:
            date = datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%S')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()
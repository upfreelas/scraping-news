# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class SudantribuneSpider(scrapy.Spider):
    name = 'sudantribune'
    allowed_domains = ['sudantribune.com']
    start_urls = ['http://www.sudantribune.com/spip.php?rubrique1']

    def parse(self, response):
        urls_sel = response.xpath('//*[@class="menu articles"]//h3/a/@href')
        for url in urls_sel.extract():
            yield Request(response.urljoin(url),
                          callback=self.parse_news)
        next_sel = response.xpath('//a[text()="next page"]/@href')
        if next_sel:
            next_url = next_sel.extract_first()
            yield Request(response.urljoin(next_url))

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'Sudan Tribune')
        l.add_xpath('title', '//*[@id="titreArticle"]')
        l.add_xpath('author', '//*[@name="article:author"]/@content')
        l.add_xpath('description', '//meta[@name="description"]/@content')
        image = response.xpath('//*[@id="text"]//img/@src')
        image_url = response.urljoin(image.extract_first())
        l.add_value('images', image_url)
        l.add_xpath('body', '//*[@id="text"]/p')
        date_sel = response.xpath('//*[@class="published"]/@title')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        date_str = date_str.split('+')[0]
        try:
            date = datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%SZ')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        yield l.load_item()
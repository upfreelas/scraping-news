# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class ReutersSpider(CrawlSpider):
    name = 'reuters'
    allowed_domains = ['reuters.com']
    start_urls = ['https://www.reuters.com/',
                  'https://www.reuters.com/finance',
                  'https://www.reuters.com/finance/markets',
                  'https://www.reuters.com/news/world',
                  'https://www.reuters.com/politics',
                  'https://www.reuters.com/news/technology',
                  'https://www.reuters.com/breakingviews']

    rules = (
        Rule(LinkExtractor(allow=r'news\/archive\/'), callback='parse_archive', follow=True),
        Rule(LinkExtractor(allow=r'article\/\S+-id\S{11}$'), callback='parse_news', follow=True),
    )

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'Reuters')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('body', '//*[@class="StandardArticleBody_body"]/p')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('title', '//*[@class="ArticleHeader_headline"]')
        l.add_xpath('author', '//meta[@property="og:article:author"]/@content')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')
        l.add_xpath('description', 'normalize-space(//*[@itemprop="description"]/text())')
        date_sel = response.xpath('//*[@property="og:article:published_time"]/@content')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        try:
            date = datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%S%z')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()

    def parse_archive(self, response):
        urls_sel = response.xpath('//*[@class="story-content"]/a/@href')
        for url in urls_sel.extract():
            yield Request(response.urljoin(url),
                          callback=self.parse_news)
# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class TheEastAfricanSpider(CrawlSpider):
    name = 'theeastafrican'
    allowed_domains = ['theeastafrican.co.ke']
    start_urls = ['https://theeastafrican.co.ke/']
    allow = [r'news\/',
             r'business\/',
             r'oped\/',
             r'scienceandhealth\/',
             r'sports\/']
    rules = (
        Rule(LinkExtractor(allow=allow), callback='parse_news', follow=True),
    )

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'The East African')
        l.add_xpath('title', '//*[@property="og:title"]/@content')
        l.add_xpath('author', '//*[@class="author noprint"]/strong')
        l.add_xpath('description', '//*[@property="og:description"]/@content')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('body', '//*[@class="summary"]')
        l.add_xpath('body', '//*[@class="body-copy"]//p[text()]')
        date_sel = response.xpath('//meta[@property="og:article:published_time"]/@content')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        date_str = date_str.split('+')[0]
        try:
            date = datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        yield l.load_item()

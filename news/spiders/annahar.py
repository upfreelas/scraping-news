# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class AnNaharSpider(CrawlSpider):
    name = 'annahar'
    allowed_domains = ['en.annahar.com']
    start_urls = ['https://en.annahar.com/']

    rules = (
        Rule(LinkExtractor(allow=r'^https:\/\/en.annahar.com\/article\/\d{6}'), callback='parse_news', follow=True),
    )

    def parse_authors_page(self, response):
        urls_sel = response.xpath('//a[starts-with(@href, "/article/")]/@href')
        for url in urls_sel.extract():
            yield Request(response.urljoin(url),
                          callback=self.parse_news)

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'An-Nahar')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('body', '//*[@class="text"]/p[text()]')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('title', './/*[contains(@class, "article_intro")]/h1')
        l.add_xpath('author', '//@data-author')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')
        date_sel = response.xpath('//*[@itemprop="datePublished"]/@content')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        date_str = date_str.split('+')[0]
        try:
            date = datetime.strptime(date_str, '%Y-%m-%dT%H:%M')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        yield l.load_item()

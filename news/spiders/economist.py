# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class EconomistSpider(scrapy.Spider):
    name = 'economist'
    allowed_domains = ['economist.com']
    start_urls = ['https://www.economist.com/latest/']

    def parse(self, response):
        urls_sel = response.xpath('//article/a/@href')
        for url in urls_sel.extract():
            print(response.urljoin(url))
            yield Request(response.urljoin(url),
                          callback=self.parse_news)
        next_sel =  response.xpath('//*[@class="pagination"]//a[text()="Next"]/@href')
        if next_sel:
            next_url = next_sel.extract_first()
            yield Request(response.urljoin(next_url))

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'The Economist')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('author', '//meta[@itemprop="author"]/@content')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        images_url = response.xpath('//*[@class="blog-post__text"]//img/@src')
        if images_url:
            for img in images_url.extract():
                l.add_value('images', response.urljoin(img))
        l.add_xpath('body', '//*[@class="blog-post__text"]/p')
        l.add_xpath('description', '//*[@property="og:description"]/@content')
        date_sel = response.xpath('//time[@class="blog-post__datetime"]/@datetime')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        try:
            date = datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%SZ')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()
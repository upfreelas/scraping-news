# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class JPostSpider(CrawlSpider):
    name = 'jpost'
    allowed_domains = ['jpost.com']
    start_urls = ['https://www.jpost.com/']

    rules = (
        Rule(LinkExtractor(allow=r'-\d+$'), callback='parse_news', follow=True),
    )

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'The Jerusalem Post')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('body', '//*[@class="article-content"]/p')
        l.add_xpath('body', '//*[@class="article-content"]')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('title', '//*[@class="article-top-box-data-title"]')
        l.add_xpath('author', '//*[@class="article-top-box-data-time-reporter"]/*[2]')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')
        l.add_xpath('description', '//*[@class="article-top-box-data-teaser"]')
        date_sel = response.xpath('//meta[@itemprop="datePublished"]/@content')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        date_str = date_str.split('+')[0]
        try:
            date = datetime.strptime(date_str, '%Y-%m-%dT%H:%MZ')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()

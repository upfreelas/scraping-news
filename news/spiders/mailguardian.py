# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class MailGuardianSpider(CrawlSpider):
    name = 'mailguardian'
    allowed_domains = ['mg.co.za']
    start_urls = ['http://www.mg.co.za/',
                  'http://mg.co.za/special-reports/']

    rules = (
        Rule(LinkExtractor(allow=r'article\/\d{4}-\d{2}-\d{2}-'), callback='parse_news', follow=True),
    )

    def parse_report(self, response):
        urls_sel = response.xpath('//*[@class="post-thumbnail"]/a/@href')
        for url in urls_sel.extract():
            yield Request(response.urljoin(url),
                          callback=self.parse_news)
        urls_sel = response.xpath('//*[@class="entry-title"]/a/@href')
        for url in urls_sel.extract():
            yield Request(response.urljoin(url),
                          callback=self.parse_news)

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'Mail & Guardian')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('title', '//*[@id="content_title"]')
        l.add_xpath('author', '//meta[@name="author"]/@content')
        l.add_xpath('author', '//*[@class="content_place_line_author"]')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('images', '//*[@class="row row-spaced"]//img/@src')
        l.add_xpath('body', '//*[@id="body_content"]/p')
        date_sel = response.xpath('//*[@class="content_place_line"]/@datetime')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        try:
            date = datetime.strptime(date_str, '%Y-%m-%d')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()

# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class HaaretzSpider(CrawlSpider):
    name = 'haaretz'
    allowed_domains = ['haaretz.com']
    start_urls = ['https://www.haaretz.com/']
    deny = ['tags/',
            '.premium-',
            'article-print-page/',
            'writers/',
            ]
    rules = (
        Rule(LinkExtractor(allow=r'.\d+$', deny=deny), callback='parse_news', follow=True),
    )

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'Haaretz')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('body', '//*[@id="article-entry"]/p')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('title', '//*[@id="art-headline"]')
        l.add_xpath('author', '//meta[@name="author"]/@content')
        l.add_xpath('description', 'normalize-space(//*[@itemprop="description"]/text())')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')
        date_sel = response.xpath('//meta[@property="article:published"]/@content')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        date_str = date_str.split('+')[0]
        try:
            date = datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%S')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()


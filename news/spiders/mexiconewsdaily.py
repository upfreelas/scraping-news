# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class MexicoNewsDailySpider(scrapy.Spider):
    name = 'mexiconewsdaily'
    allowed_domains = ['mexiconewsdaily.com']
    start_urls = ['https://mexiconewsdaily.com/category/news/']

    def parse(self, response):
        urls_sel = response.xpath('//article//a/@href')
        for url in urls_sel.extract():
            yield Request(response.urljoin(url),
                          callback=self.parse_news)
        next_sel = response.xpath('//a[text()="Older posts"]/@href')
        if next_sel:
            next_url = next_sel.extract_first()
            yield Request(response.urljoin(next_url))

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'Mexico News Daily')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('body', '//*[@class="pigeon-remove"]//p')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('title', '//*[@class="entry-title"]')
        l.add_xpath('author', '//meta[@name="author"]/@content')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')
        l.add_xpath('description', '//meta[@name="twitter:description"]/@content')
        date_sel = response.xpath('//meta[@property="article:published_time"]/@content')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        date_str = date_str.rsplit('-', 1)[0]
        try:
            date = datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%S')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()
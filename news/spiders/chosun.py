# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class ChosunSpider(scrapy.Spider):
    name = 'chosun'
    allowed_domains = ['chosun.com']
    start_urls = ['http://english.chosun.com/svc/list_in/list.html?catid=1',
                  'http://english.chosun.com/svc/list_in/list.html?catid=F',
                  'http://english.chosun.com/svc/list_in/list.html?catid=2',
                  'http://english.chosun.com/svc/list_in/list.html?catid=3',
                  'http://english.chosun.com/svc/list_in/list.html?catid=4',
                  'http://english.chosun.com/svc/list_in/list.html?catid=G']

    def parse(self, response):
        urls_sel = response.xpath('//*[@id="list_area"]//dt/a/@href')
        for url in urls_sel.extract():
            yield Request(response.urljoin(url),
                          callback=self.parse_news)
        next_sel = response.xpath('//*[@class="next"]/a/@href')
        if next_sel:
            next_url = next_sel.extract_first()
            yield Request(response.urljoin(next_url))
    
    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'Chosun')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('author', 'normalize-space(//*[@id="j1"])')
        l.add_xpath('body', '//*[@class="par"]')
        date_sel = response.xpath('normalize-space(//*[@id="date_text"])')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        try:
            date = datetime.strptime(date_str, '%B %d, %Y %H:%M')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        yield l.load_item()
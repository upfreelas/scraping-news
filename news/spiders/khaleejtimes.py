# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class KhaleejTimesSpider(scrapy.Spider):
    name = 'khaleejtimes'
    allowed_domains = ['khaleejtimes.com']
    start_urls = ['https://www.khaleejtimes.com/news']
    
    def __init__(self, *args, **kwargs):
        self.PAGE = 1
        super(KhaleejTimesSpider, self).__init__(*args, **kwargs)

    def parse(self, response):
        urls_sel = response.xpath('//*[@class="liting_list"]//h2/a/@href')
        for url in urls_sel.extract():
            yield Request(response.urljoin(url),
                          callback=self.parse_news)
        self.PAGE += 1
        next_url = 'https://www.khaleejtimes.com/news&pagenumber=%s' % self.PAGE
        yield Request(next_url)
    
    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'Khaleej Times')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('author', '//meta[@name="author"]/@content')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('body', '//p[@itemprop="articleBody"]')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')        
        date_sel = response.xpath('normalize-space(//*[@class="author_detail"])')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        date_str = date_str.split(' | ')[0]
        try:
            date = datetime.strptime(date_str, 'Filed on %B %d, %Y')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()
# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class EgyptIndependentSpider(scrapy.Spider):
    name = 'egyptindependent'
    allowed_domains = ['egyptindependent.com']
    start_urls = ['https://ww.egyptindependent.com/category/egypt/',
                  'https://ww.egyptindependent.com/category/world/',
                  'https://ww.egyptindependent.com/category/middle_east/',
                  'https://ww.egyptindependent.com/category/business/',
                  'https://ww.egyptindependent.com/category/science/']

    def parse(self, response):
        urls_sel = response.xpath('//*[@class="main"]//h5/a/@href')
        for url in urls_sel.extract():
            yield Request(response.urljoin(url),
                          callback=self.parse_news)
        next_sel = response.xpath('//*[text()="Next"]/@href')
        if next_sel:
            next_url = next_sel.extract_first()
            yield Request(response.urljoin(next_url))

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'Egypt Independent')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('author', '//*[contains(@class, "author-box")]/h2')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('body', '//*[@class="main"]//p[not(@*)]')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')        
        date_sel = response.xpath('normalize-space(//time/text())')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        try:
            date = datetime.strptime(date_str, '%B %d, %Y')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()

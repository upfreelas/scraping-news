# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class AsahiSpider(CrawlSpider):
    name = 'asahi'
    allowed_domains = ['asahi.com']
    start_urls = ['http://www.asahi.com/ajw/',
                  'http://www.asahi.com/ajw/all-news/',
                  'http://www.asahi.com/ajw/japan/',
                  'http://www.asahi.com/ajw/japan/social/',
                  'http://www.asahi.com/ajw/japan/0311disaster/',
                  'http://www.asahi.com/ajw/japan/people/',
                  'http://www.asahi.com/ajw/japan/sci_tech/',
                  'http://www.asahi.com/ajw/politics/',
                  'http://www.asahi.com/ajw/business/',
                  'http://www.asahi.com/ajw/sports/',
                  'http://www.asahi.com/ajw/culture/',
                  'http://www.asahi.com/ajw/culture/style/',
                  'http://www.asahi.com/ajw/culture/movies/',
                  'http://www.asahi.com/ajw/culture/manga_anime/',
                  'http://www.asahi.com/ajw/travel/',
                  'http://www.asahi.com/ajw/asia/',
                  'http://www.asahi.com/ajw/asia/china/'
                  'http://www.asahi.com/ajw/asia/korean_peninsula/',
                  'http://www.asahi.com/ajw/asia/around_asia/',
                  'http://www.asahi.com/ajw/opinion/',
                  'http://www.asahi.com/ajw/opinion/editorial/',
                  'http://www.asahi.com/ajw/opinion/vox/',
                  'http://www.asahi.com/ajw/opinion/views/',
                  'http://www.asahi.com/ajw/special/']

    rules = (
        Rule(LinkExtractor(allow=r'articles/', deny=r'photo/'), callback='parse_news', follow=True),
    )

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'Ashahi Shimbun')
        l.add_xpath('images', '//*[@property="og:image"]/@content')
        l.add_xpath('images', '//*[@class="Image"]//a/img/@src')
        l.add_xpath('body', '//*[@class="ArticleText"]/p')
        l.add_xpath('title', '//*[@property="og:title"]/@content')
        l.add_xpath('title', '//*[@class="Title"]/h1')
        l.add_xpath('author', '//*[@class="EnArticleName"]')
        l.add_xpath('description', '//*[@property="og:description"]/@content')
        l.add_xpath('description', '//*[@name="description"]/@content')
        date_sel = response.xpath('//*[@class="EnLastUpdated"]/text()')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        try:
            date = datetime.strptime(date_str, '%B %d, %Y at %H:%M JST')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))

        return l.load_item()

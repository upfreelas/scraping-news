# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class RiotimesonlineSpider(scrapy.Spider):
    name = 'riotimesonline'
    allowed_domains = ['riotimesonline.com']
    start_urls = ['http://riotimesonline.com/brazil-news/category/rio-politics/']

    def parse(self, response):
        urls_sel = response.xpath('//a[@rel="bookmark"]/@href')
        for url in urls_sel.extract():
            yield Request(response.urljoin(url),
                          callback=self.parse_news)
        next_sel = response.xpath('//*[contains(@class, "page-nav")]/a[last()]/i/parent::a/@href')
        if next_sel:
            next_url = next_sel.extract_first()
            yield Request(next_url)

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('source', 'The Rio Times')
        l.add_value('url', response.url)
        l.add_xpath('title', '//header[@class="td-post-title"]/h1')
        l.add_xpath('author', '//*[@class="td-post-author-name"]/*[2]')
        date_sel = response.xpath('//header//time/text()')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        try:
            date = datetime.strptime(date_str, '%B %d, %Y')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        images_url = response.xpath('//*[@class="td-post-content"]//img/@src')
        if images_url:
            for img in images_url.extract():
                l.add_value('images', response.urljoin(img))
        l.add_xpath('body', '//*[@class="td-post-content"]/p')
        return l.load_item()
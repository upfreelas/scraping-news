# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class TheMoscowTimesSpider(scrapy.Spider):
    name = 'themoscowtimes'
    allowed_domains = ['themoscowtimes.com']
    start_urls = ['https://themoscowtimes.com/allnews/1']

    def __init__(self, *args, **kwargs):
        self.PAGE = 1
        super(TheMoscowTimesSpider, self).__init__(*args, **kwargs)

    def parse(self, response):
        urls_sel = response.xpath('//*[@class="info js-info"]//a/@href')
        for url in urls_sel.extract():
            yield Request(response.urljoin(url),
                          callback=self.parse_news)
        self.PAGE += 1
        next_url = 'https://themoscowtimes.com/allnews/%s' % self.PAGE
        yield Request(next_url)


    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'The Moscow Times')
        l.add_xpath('title', '//meta[@property="og:title"]/@content')
        l.add_xpath('title', '//*[@class="tmt-news-title"]')
        l.add_xpath('author', '//*[@class="tmt-news-author__name"]/a')
        l.add_xpath('description', '//meta[@property="og:description"]/@content')
        l.add_xpath('images', '//*[@class="tmt-news-picture__img"]/img/@src')
        l.add_xpath('body', '//*[@class="tmt-news-text"]/p')
        date_sel = response.xpath('//*[@class="tmt-news-meta__date"]/text()')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        try:
            date = datetime.strptime(date_str, '%b. %d %Y - %H:%M')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()


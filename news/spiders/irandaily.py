# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class IranDailySpider(scrapy.Spider):
    name = 'irandaily'
    allowed_domains = ['iran-daily.com']
    start_urls = ['http://www.iran-daily.com/News/Archive?catid=2&type=ServiceLatest',
                  'http://www.iran-daily.com/News/Archive?catid=3&type=ServiceLatest',
                  'http://www.iran-daily.com/News/Archive?catid=4&type=ServiceLatest',
                  'http://www.iran-daily.com/News/Archive?catid=5&type=ServiceLatest',
                  'http://www.iran-daily.com/News/Archive?catid=6&type=ServiceLatest',
                  'http://www.iran-daily.com/News/Archive?catid=8&type=ServiceLatest',
                  'http://www.iran-daily.com/News/Archive?catid=9&type=ServiceLatest',
                  'http://www.iran-daily.com/News/Archive?catid=10&type=ServiceLatest',
                  'http://www.iran-daily.com/News/Archive?catid=11&type=ServiceLatest',
                  'http://www.iran-daily.com/News/Archive?catid=12&type=ServiceLatest',
                  'http://www.iran-daily.com/News/Archive?catid=13&type=ServiceLatest',
                  'http://www.iran-daily.com/News/Archive?catid=14&type=ServiceLatest']

    def parse(self, response):
        urls_sel = response.xpath('//*[@class="titr"]/a/@href')
        for url in urls_sel.extract():
            yield Request(response.urljoin(url),
                          callback=self.parse_news)
        next_sel = response.xpath('//*[@class="PagedList-skipToNext"]/a/@href')
        if next_sel:
            next_url = next_sel.extract_first()
            yield Request(response.urljoin(next_url))
    
    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'Iran Daily')
        l.add_xpath('title', '//h1[@class="titr"]/a')
        l.add_xpath('author', '//meta[@name="author"]/@content')
        main_image = response.xpath('//*[@id="newsmainimage"]/@href').extract_first()
        l.add_value('images', response.urljoin(main_image))
        images_url = response.xpath('//*[@class="itemcontent"]//img/@src')
        if images_url:
            for img in images_url.extract():
                l.add_value('images', response.urljoin(img))        
        l.add_xpath('body', '//*[@class="itemcontent"]/p[text()]')
        l.add_xpath('description', '//*[@class="lide"]')        
        date_sel = response.xpath('normalize-space(//*[@class="info"]/*[2]/text())')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        try:
            date = datetime.strptime(date_str, 'Published: %H%M GMT %B %d, %Y')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        return l.load_item()

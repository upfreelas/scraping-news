# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from news.items import NewsItem
from scrapy.loader import ItemLoader
from datetime import datetime


class SCMPSpider(CrawlSpider):
    name = 'scmp'
    allowed_domains = ['scmp.com']
    start_urls = ['https://www.scmp.com/frontpage/international/']
    deny_domains= ['register.scmp.com',
                   'yp.scmp.com']
    rules = (
        Rule(LinkExtractor(allow=r'article\/', deny_domains=deny_domains), callback='parse_news', follow=True),
    )

    def parse_news(self, response):
        l = ItemLoader(item=NewsItem(), response=response)
        l.add_value('url', response.url)
        l.add_value('source', 'South China Morning Post')
        l.add_xpath('title', '//*[@property="og:title"]/@content')
        l.add_xpath('author', '//*[@name="article:author"]/@content')
        l.add_xpath('description', '//*[@property="og:description"]/@content')
        l.add_xpath('images', '//meta[@property="og:image"]/@content')
        l.add_xpath('body', '//*[@class="pane-content"]/p[text()]')
        date_sel = response.xpath('//*[@itemprop="dateCreated"]/@datetime')
        if not date_sel:
            # Will be dropped on the item pipeline
            return l.load_item()
        date_str = date_sel.extract_first()
        date_str = date_str.split('+')[0]
        try:
            date = datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%S')
        except ValueError:
            # Will be dropped on the item pipeline
            return l.load_item()
        l.add_value('date', date.strftime('%m/%d/%Y'))
        yield l.load_item()
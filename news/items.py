# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import MapCompose, TakeFirst, Join
from w3lib.html import remove_tags


def normalize_space(value):
    """
    Normalizes spaces in fields.
    """
    return " ".join(value.split())

def remove_empty(s):
    """
    Returns None if string is empty, otherwise string itself
    """
    return s if s else None


class NewsItem(scrapy.Item):
    title = scrapy.Field(input_processor=MapCompose(normalize_space, remove_tags),
    					 output_processor=TakeFirst())
    info = scrapy.Field(input_processor=MapCompose(normalize_space),
    					output_processor=TakeFirst())
    images = scrapy.Field()
    url = scrapy.Field(output_processor=TakeFirst())
    source = scrapy.Field(output_processor=TakeFirst())
    author = scrapy.Field(input_processor=MapCompose(normalize_space, remove_tags),
    					  output_processor=TakeFirst())
    date = scrapy.Field(output_processor=TakeFirst())
    body = scrapy.Field(input_processor=MapCompose(normalize_space, remove_tags, remove_empty))
    description = scrapy.Field(input_processor=MapCompose(normalize_space, remove_tags, remove_empty),
                               output_processor=TakeFirst())
# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
from scrapy.exceptions import DropItem


class NewsPipeline(object):
    def process_item(self, item, spider):
        if item['date']:
            return item
        else:
            raise DropItem("Page does not have date %s" % item)
        if item['author']:
            return item
        else:
            raise DropItem("Page does not have author %s" % item)
